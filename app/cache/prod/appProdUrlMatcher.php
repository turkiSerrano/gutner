<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // srk_slide_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'srk_slide_homepage')), array (  '_controller' => 'Srk\\SlideBundle\\Controller\\DefaultController::indexAction',));
        }

        // jcg_accueil
        if ($pathinfo === '/index') {
            return array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\mainController::indexAction',  '_route' => 'jcg_accueil',);
        }

        if (0 === strpos($pathinfo, '/reportage')) {
            // jcg_reportages
            if ($pathinfo === '/reportages') {
                return array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\mainController::reportagesAction',  '_route' => 'jcg_reportages',);
            }

            // jcg_reportage
            if (preg_match('#^/reportage/(?P<url>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jcg_reportage')), array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\mainController::reportageAction',));
            }

        }

        // jcg_biographie
        if ($pathinfo === '/biographie') {
            return array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\mainController::biographieAction',  '_route' => 'jcg_biographie',);
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // jcg_admin
            if (preg_match('#^/admin(?:/(?P<url>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jcg_admin')), array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\adminController::actionAction',  'url' => 'index',));
            }

            // jcg_admin_photo
            if (0 === strpos($pathinfo, '/admin/photo') && preg_match('#^/admin/photo(?:/(?P<url>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'jcg_admin_photo')), array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\adminController::photoAction',  'url' => 'index',));
            }

        }

        if (0 === strpos($pathinfo, '/login')) {
            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'Jcg\\GutnerBundle\\Controller\\adminController::loginAction',  '_route' => 'login',);
            }

            // login_check
            if ($pathinfo === '/login_check') {
                return array('_route' => 'login_check');
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
