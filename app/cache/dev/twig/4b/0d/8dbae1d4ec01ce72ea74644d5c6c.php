<?php

/* JcgGutnerBundle:Default:biographie.html.twig */
class __TwigTemplate_4b0d8dbae1d4ec01ce72ea74644d5c6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
<article class=\"biographie\">
    <h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["biographie"]) ? $context["biographie"] : $this->getContext($context, "biographie")), "titre"), "html", null, true);
        echo "</h2><br>
    <p class=\"bioContenu\">";
        // line 9
        echo $this->getAttribute((isset($context["biographie"]) ? $context["biographie"] : $this->getContext($context, "biographie")), "contenu");
        echo "</p>
</article>

";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:biographie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  43 => 8,  39 => 6,  36 => 5,  29 => 3,);
    }
}
