<?php

/* JcgGutnerBundle::layout.html.twig */
class __TwigTemplate_fd3b5339d59d24b8bf92fb875efdfad5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html lang=\"fr\">
    ";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 26
        echo "        <body>
            <header id=\"header\">
                <div id=\"conteneurHeader\">
                    <div id=\"logo\">
                        <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("jcg_accueil");
        echo "\">Jeans Charles Gutner - Reportages</a>
                    </div>
                    <nav id=\"menu\">
                        <ul>
                            <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("jcg_reportages");
        echo "\" >Reportages</a></li>
                            <li><a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("jcg_biographie");
        echo "\">Biographie</a></li>
                            <li><a href=\"#\">Book</a></li>
                            <li><a href=\"#\">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
            <div id=\"conteneur\">
                <div>
                    ";
        // line 44
        $this->displayBlock('body', $context, $blocks);
        // line 48
        echo "

                    </div>



                </div>


                <div id=\"conteneurFooter\">
                    <footer id=\"footer\">
                        <p><span class=\"copyright\">©</span> JC Gutner 2013</p>
                    </footer>
                </div>



            </body>
        </html>";
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "        <head>
            <meta charset=\"utf-8\">
            <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('javascript', $context, $blocks);
        // line 24
        echo "        </head>
    ";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Gutner Reportages, Photographies";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/global.css"), "html", null, true);
        echo "\" type=\"text/css\">
            <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/srkslide/css/slides.css"), "html", null, true);
        echo "\" type=\"text/css\">
            <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fancybox/jquery.fancybox.css"), "html", null, true);
        echo "\" type=\"text/css\">
            <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/favicon.png"), "html", null, true);
        echo "\" />
            

         ";
    }

    // line 16
    public function block_javascript($context, array $blocks = array())
    {
        // line 17
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.10.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
            <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
            <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fancybox/jquery.fancybox.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fancybox/jquery.fancybox.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/srkslide/js/slide.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
            <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/jcggutner/js/reportage.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        ";
    }

    // line 44
    public function block_body($context, array $blocks = array())
    {
        // line 45
        echo "

                    ";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 45,  166 => 44,  160 => 22,  156 => 21,  152 => 20,  148 => 19,  144 => 18,  139 => 17,  136 => 16,  128 => 12,  124 => 11,  120 => 10,  116 => 9,  113 => 8,  110 => 7,  104 => 6,  99 => 24,  96 => 16,  94 => 7,  90 => 6,  86 => 4,  83 => 3,  61 => 48,  59 => 44,  47 => 35,  43 => 34,  36 => 30,  30 => 26,  28 => 3,  24 => 1,);
    }
}
