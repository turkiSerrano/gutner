<?php

/* JcgGutnerBundle:Default:adminReportages.html.twig */
class __TwigTemplate_565bcc207d8f3d9abe6d7679dbc9e500 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'retour' => array($this, 'block_retour'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Administration ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<section class=\"adminSection file-arianne\">
";
        // line 8
        $this->displayBlock('retour', $context, $blocks);
        // line 11
        echo "</section>
<section class=adminSection>

    <hgroup>
        <h2>Choississez un reportage </h2>
        <h3>Liste des reportages présent sur le site </h3>
    </hgroup>
    <nav>
        <ul id=\"listReportageAdmin\">
";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listReportages"]) ? $context["listReportages"] : $this->getContext($context, "listReportages")));
        foreach ($context['_seq'] as $context["_key"] => $context["reportage"]) {
            // line 21
            echo "                <li>
                    <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
            echo "</a>
                </li>   
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reportage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "            </ul>
        </nav>
    </section>

";
    }

    // line 8
    public function block_retour($context, array $blocks = array())
    {
        // line 9
        echo "<img class=\"imgarianne\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/fleche_retour.png"), "html", null, true);
        echo "\"> <a class=\"arianne\" href=\"";
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "index"));
        echo "\">Retour à l'index de l'administration</a>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:adminReportages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 9,  82 => 8,  74 => 25,  63 => 22,  60 => 21,  56 => 20,  45 => 11,  43 => 8,  40 => 7,  37 => 6,  30 => 3,);
    }
}
