<?php

/* JcgGutnerBundle:Default:adminIndex.html.twig */
class __TwigTemplate_9abc60bb888163053ef9dfd267913c09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'retour' => array($this, 'block_retour'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Administration Présentation index ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
 
";
        // line 9
        $this->displayBlock('retour', $context, $blocks);
        // line 14
        echo "</section>
<section class=adminSection>
    <h2>Modifier votre Présentation d'accueil</h2>
    ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contenu"), 'row');
        echo "
    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</section>
 ";
        // line 22
        echo $this->env->getExtension('stfalcon_tinymce')->tinymceInit();
        echo "
";
    }

    // line 9
    public function block_retour($context, array $blocks = array())
    {
        // line 10
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "JcgGutnerBundle:Form:fields.html.twig"));
        // line 11
        echo "<section class=\"adminSection file-arianne\">
<img class=\"imgarianne\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/fleche_retour.png"), "html", null, true);
        echo "\"> <a class=\"arianne\" href=\"";
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "index"));
        echo "\">Retour à l'index de l'administration</a>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:adminIndex.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 12,  79 => 11,  77 => 10,  74 => 9,  68 => 22,  63 => 20,  59 => 19,  55 => 18,  51 => 17,  46 => 14,  44 => 9,  40 => 7,  37 => 6,  30 => 3,);
    }
}
