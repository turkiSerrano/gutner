<?php

/* JcgGutnerBundle:Default:adminReportage.html.twig */
class __TwigTemplate_70aaafc67f9c99ea48bb2361482876fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'retour' => array($this, 'block_retour'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "JcgGutnerBundle:Form:fields.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Administration ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<section class=\"adminSection file-arianne\">
";
        // line 8
        $this->displayBlock('retour', $context, $blocks);
        // line 11
        echo "    </section>
    <section class=\"adminSection\">


        <h2>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
        echo "</h2>
    ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intro"), 'row');
        echo "
        ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date"), 'row');
        echo "
        ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "url"), 'row');
        echo "
        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'row');
        echo "
    ";
        // line 23
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

        <ul class=\"champsReportages\">
            <li>
                <p>Photo utilisé pour le slide (acceuil du site) : </p>
                <img class=\"imageAdmin\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "src") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "nom")) . ".") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "extension"))), "html", null, true);
        echo "\"/>
                <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_admin_photo", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
        echo "\">modifier la photo</a>
            </li>
            <li>
                <p>Photo utilisé pour la presentation du reportage  : </p>
                <img class=\"imageAdmin\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "src") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "nom")) . ".") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "extension"))), "html", null, true);
        echo "\"/>
                <a href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_admin_photo", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
        echo "\">modifier la photo</a>
            </li>
            
        </ul> 
    </section>
";
        // line 39
        echo $this->env->getExtension('stfalcon_tinymce')->tinymceInit();
        echo "
";
    }

    // line 8
    public function block_retour($context, array $blocks = array())
    {
        // line 9
        echo "        <img class=\"imgarianne\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/fleche_retour.png"), "html", null, true);
        echo "\"> <a class=\"arianne\" href=\"";
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifReportages"));
        echo "\">Retour à la liste des reportages</a>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:adminReportage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 9,  119 => 8,  113 => 39,  105 => 34,  101 => 33,  94 => 29,  90 => 28,  82 => 23,  78 => 22,  74 => 21,  70 => 20,  66 => 19,  61 => 17,  57 => 16,  53 => 15,  47 => 11,  45 => 8,  42 => 7,  39 => 6,  32 => 3,  27 => 4,);
    }
}
