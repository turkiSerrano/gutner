<?php

/* JcgGutnerBundle:Default:adminPhoto.html.twig */
class __TwigTemplate_47f2e5b5b15ca0732267a367527ab082 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascript($context, array $blocks = array())
    {
        // line 3
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.10.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/admin.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  ";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
        echo "  ";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<section id=\"mainContent\">
    <hgroup>
        <h1 class=\"titreReportage\">";
        // line 14
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre")), "html", null, true);
        echo "</h1>
        <h2>choisissez une photo</h2>
    </hgroup>
    ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'row');
        echo "
        ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contenu"), 'row');
        echo "
    
   ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeImages"]) ? $context["listeImages"] : $this->getContext($context, "listeImages")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 24
            echo "        ";
            if ((($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") % 6) == 0)) {
                // line 25
                echo "    <img class=\"photoAdmin\" style=\"margin-right: 0;\" id=\"miniature\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . "min/") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . "_min.") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\">
            ";
            } else {
                // line 27
                echo "    <img class=\"photoAdmin\" id=\"miniature\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . "min/") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . "_min.") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\">
    ";
            }
            // line 29
            echo "   ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</section>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:adminPhoto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 30,  121 => 29,  115 => 27,  109 => 25,  106 => 24,  89 => 23,  84 => 21,  80 => 20,  75 => 18,  71 => 17,  65 => 14,  61 => 12,  58 => 11,  49 => 9,  42 => 5,  38 => 4,  33 => 3,  30 => 2,);
    }
}
