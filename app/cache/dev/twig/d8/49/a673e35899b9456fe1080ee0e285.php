<?php

/* JcgGutnerBundle:Default:reportages.html.twig */
class __TwigTemplate_d849a673e35899b9456fe1080ee0e285 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Reportages ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
<section id=\"mainContent\">
";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeArticles"]) ? $context["listeArticles"] : $this->getContext($context, "listeArticles")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["reportage"]) {
            // line 10
            echo "
    ";
            // line 11
            if ((($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") % 3) == 0)) {
                // line 12
                echo "        <article class=\"reportagePresentation\" style=\"margin-right: 0px\" >
            <a href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
                echo "\">
                <img src=\"";
                // line 14
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "src") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "nom")) . ".") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "extension"))), "html", null, true);
                echo "\">
            </a>
            <nav>   
                <a href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
                echo "</a>
            </nav>
            <aside>
                <p>";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "intro"), "html", null, true);
                echo "</p>
            </aside>
        </article>
        <div class=\"clear\"><div>
";
            } else {
                // line 25
                echo "        <article class=\"reportagePresentation\" >
            <a href=\"";
                // line 26
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
                echo "\">
                <img src=\"";
                // line 27
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((($this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "src") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "image"), "nom")) . ".jpg")), "html", null, true);
                echo "\">
            </a>
            <nav>
                <a href=\"";
                // line 30
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
                echo "</a>
            </nav>
            <aside>
                <p>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "intro"), "html", null, true);
                echo "</p>
            </aside>
        </article>
        ";
            }
            // line 37
            echo "

";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reportage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "

";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:reportages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 40,  122 => 37,  115 => 33,  107 => 30,  101 => 27,  97 => 26,  94 => 25,  86 => 20,  78 => 17,  72 => 14,  68 => 13,  65 => 12,  63 => 11,  60 => 10,  43 => 9,  39 => 7,  36 => 6,  29 => 4,);
    }
}
