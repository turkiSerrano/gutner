<?php

/* JcgGutnerBundle:Form:fields.html.twig */
class __TwigTemplate_bff7527f2981f98f17904c658e93bc89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_row' => array($this, 'block_form_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('form_row', $context, $blocks);
    }

    public function block_form_row($context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo "    <div class=\"form_row\">
        <p class=\"label\">";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        echo "</p>
        <div class=\"formErreur\">";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "</div>
        <div class=\"widget\">";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "</div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  35 => 5,  31 => 4,  28 => 3,  26 => 2,  20 => 1,  87 => 12,  84 => 11,  82 => 10,  79 => 9,  73 => 24,  68 => 22,  64 => 21,  60 => 20,  55 => 18,  51 => 17,  46 => 14,  44 => 9,  40 => 7,  37 => 6,  30 => 3,);
    }
}
