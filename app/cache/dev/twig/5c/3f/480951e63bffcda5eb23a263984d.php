<?php

/* JcgGutnerBundle:Default:reportage.html.twig */
class __TwigTemplate_5c3f480951e63bffcda5eb23a263984d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
        echo "  ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<section id=\"mainContent\">
    <h1 class=\"titreReportage\">";
        // line 8
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre")), "html", null, true);
        echo "</h1>
   ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeImages"]) ? $context["listeImages"] : $this->getContext($context, "listeImages")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 10
            echo "        ";
            if ((($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") % 6) == 0)) {
                // line 11
                echo "    <a  class=\"groupImage\"  rel=\"fancybox-thumb\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . ".") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\">
        <img style=\"margin-right: 0;\" id=\"miniature\" src=\"";
                // line 12
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . "min/") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . "_min.") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\"></a>
            ";
            } else {
                // line 14
                echo "    <a  class=\"groupImage\"  rel=\"fancybox-thumb\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . ".") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\">
        <img id=\"miniature\" src=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((((($this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "src") . "min/") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "nom")) . "_min.") . $this->getAttribute((isset($context["image"]) ? $context["image"] : $this->getContext($context, "image")), "extension"))), "html", null, true);
                echo "\"></a>
    ";
            }
            // line 17
            echo "   ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <div class=\"clear\"></div>
    <section id=\"presentation_reportage\">
        <p>
            ";
        // line 22
        echo $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "contenu");
        echo "
            </p>
        </section>
    </section>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:reportage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 22,  102 => 18,  88 => 17,  83 => 15,  78 => 14,  73 => 12,  68 => 11,  65 => 10,  48 => 9,  44 => 8,  41 => 7,  38 => 6,  29 => 4,);
    }
}
