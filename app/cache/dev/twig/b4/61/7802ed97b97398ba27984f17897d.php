<?php

/* JcgGutnerBundle:Default:index.html.twig */
class __TwigTemplate_b4617802ed97b97398ba27984f17897d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<div id=\"mainSlideContener\">
";
        // line 7
        $this->env->loadTemplate("SrkSlideBundle:Default:slide.html.twig")->display($context);
        // line 8
        echo "</div>


<section id=\"presentation\">
    <p> <span class=\"pseudo-titre\">Jean Charles Gutner</span> : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["presentation"]) ? $context["presentation"] : $this->getContext($context, "presentation")), "contenu"), "html", null, true);
        echo "</p>
</section>
";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 12,  44 => 8,  42 => 7,  39 => 6,  36 => 5,  29 => 3,);
    }
}
