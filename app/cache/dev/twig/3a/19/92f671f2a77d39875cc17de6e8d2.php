<?php

/* SrkSlideBundle:Default:slide.html.twig */
class __TwigTemplate_3a1992f671f2a77d39875cc17de6e8d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"precedent\" ></div>
<div id=\"suivant\" ></div>

<section id=\"slides\">

    

    <div id=\"contenantSlider\">
        
        
        ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeArticles"]) ? $context["listeArticles"] : $this->getContext($context, "listeArticles")));
        foreach ($context['_seq'] as $context["_key"] => $context["reportage"]) {
            // line 12
            echo "        <article class=\"articleSlide\">
            <a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
            echo "\" >
                <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((($this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "src") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "nom")) . ".") . $this->getAttribute($this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "imageSlide"), "extension"))), "html", null, true);
            echo "\" alt=\"\">
            </a>
            <div>
                <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("jcg_reportage", array("url" => $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "url"))), "html", null, true);
            echo "\" title=\"\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportage"]) ? $context["reportage"] : $this->getContext($context, "reportage")), "titre"), "html", null, true);
            echo "</a>
            </div>
        </article>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reportage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "        
        
        
     <!--  <article class=\"articleSlide\">
            <a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("jcg_reportages");
        echo "\" ><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/clin-d-oeil.jpg"), "html", null, true);
        echo "\" alt=\"\"></a>
            <div>
                <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("jcg_reportages");
        echo "\" title=\"\">Clin d'oeil</a>
            </div>
        </article>
        <article class=\"articleSlide\">
            <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("jcg_reportages");
        echo "\" ><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/angola_slide.jpg"), "html", null, true);
        echo "\" alt=\"\"></a>
            <div>
                <a href=\"\" title=\"\">Angola</a>
            </div>
        </article>
        <article class=\"articleSlide\">
            <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("jcg_reportages");
        echo "\" ><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/in-vino-luminis.jpg"), "html", null, true);
        echo "\" alt=\"\"></a>
            <div>
                <a href=\"\" title=\"\">In Vino Luminis</a>
            </div>
        </article>-->
    </div>
</section>

";
    }

    public function getTemplateName()
    {
        return "SrkSlideBundle:Default:slide.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 37,  80 => 31,  73 => 27,  66 => 25,  60 => 21,  48 => 17,  38 => 13,  35 => 12,  31 => 11,  19 => 1,  170 => 46,  167 => 45,  161 => 23,  157 => 22,  153 => 21,  148 => 19,  144 => 18,  139 => 17,  136 => 16,  128 => 12,  124 => 11,  120 => 10,  116 => 9,  113 => 8,  110 => 7,  104 => 6,  99 => 25,  96 => 16,  94 => 7,  90 => 6,  86 => 4,  83 => 3,  61 => 49,  59 => 45,  47 => 36,  43 => 35,  30 => 27,  28 => 3,  24 => 1,  44 => 8,  42 => 14,  39 => 6,  36 => 31,  29 => 3,);
    }
}
