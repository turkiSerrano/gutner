<?php

/* JcgGutnerBundle:Default:admin.html.twig */
class __TwigTemplate_309e4362b4dadaf1804e6ca6856afa50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("JcgGutnerBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "JcgGutnerBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Administration ";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "
<section id=\"adminReportages\" class=\"adminSection\">
    <h2>Reportages</h2>
    <div class=\"action\">
        <a href=";
        // line 12
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "ajoutReportage"));
        echo "><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/ajouter-icone.png"), "html", null, true);
        echo "\"></a>
        <a href=";
        // line 13
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "ajoutReportage"));
        echo ">Ajouter un Reportage</a>
    </div>
    <div class=\"action\">
        <a href=";
        // line 16
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifReportages"));
        echo "><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/modifier-icone.png"), "html", null, true);
        echo "\"></a>
        <a href=";
        // line 17
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifReportages"));
        echo ">Modifier un Reportage</a>
    </div>
    <div class=\"action\">
    </div>
</section>
<section id=\"adminBio\" class=\"adminSection\">
   <h2>Biographie</h2>
   <div class=\"action\">
        <a href=";
        // line 25
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifBiographie"));
        echo "><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/modifier-icone.png"), "html", null, true);
        echo "\"></a>
        <a href=";
        // line 26
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifBiographie"));
        echo ">Modifier la Biographie</a>
   </div>
</section>
<section id=\"adminBook\" class=\"adminSection\">
    <h2>Book</h2>
    <div class=\"action\">
        <a href=";
        // line 32
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifBook"));
        echo "><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/ajouter-icone.png"), "html", null, true);
        echo "\"></a>
        <a href=";
        // line 33
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifBook"));
        echo ">Ajouter un Livre</a>
   </div>
</section>
<section id=\"adminIndex\" class=\"adminSection\">
    <h2>Accueil</h2>
    <div class=\"action\">
        <a href=";
        // line 39
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifPresentationIndex"));
        echo "><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/modifier-icone.png"), "html", null, true);
        echo "\"></a>
        <a href=";
        // line 40
        echo $this->env->getExtension('routing')->getPath("jcg_admin", array("url" => "modifPresentationIndex"));
        echo ">Modifier L'Acceuil</a>
   </div>
</section>

";
    }

    public function getTemplateName()
    {
        return "JcgGutnerBundle:Default:admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 40,  104 => 39,  95 => 33,  89 => 32,  80 => 26,  74 => 25,  63 => 17,  57 => 16,  51 => 13,  45 => 12,  39 => 8,  36 => 7,  29 => 5,);
    }
}
