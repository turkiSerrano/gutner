<?php

namespace Jcg\GutnerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class mainController extends Controller {

    public function indexAction() {

        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Reportages");
        $article = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("\Jcg\GutnerBundle\Entity\Article");
        
        $presentation = $article->findOneBy(array("is_index" => 1));
        $listeArticles = $repository->findAll();
        
        return $this->render('JcgGutnerBundle:Default:index.html.twig', array('listeArticles' => $listeArticles,'presentation'=>$presentation));
    }

    public function biographieAction(){
        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Article");
        $biographie = $repository->findOneBy(array("is_bio" => 1));
        return $this->render('JcgGutnerBundle:Default:biographie.html.twig', array('biographie' => $biographie));
    }
    
    public function reportagesAction() {

        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Reportages");

        $listeArticles = $repository->findAll();

        return $this->render('JcgGutnerBundle:Default:reportages.html.twig', array('listeArticles' => $listeArticles));
    }

    /**
     * 
     * @param String $url
     * @return Array 
     */
    public function reportageAction($url) {
        //on charge les images lie au reportages
        //selon l'url demander
        
        $listeImages = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Image")
                ->getImageAvecReportage($url);
        
        return $this->render('JcgGutnerBundle:Default:reportage.html.twig', array('reportage' => $listeImages[0]->getReportage(), 'listeImages' => $listeImages));
    }

   

}
