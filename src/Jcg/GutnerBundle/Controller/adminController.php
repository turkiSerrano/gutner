<?php

namespace Jcg\GutnerBundle\Controller;

//include_once '../class.imageResize.php';
use Jcg\GutnerBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class adminController extends Controller {

    /**
     * ajoute en base toutes photos du dossier passer en 
     * parametres
     * @param String $dirname
     */
    private function ajoutPhotos($dirname) {

        //connection a la base
        $dir = opendir($dirname);
        //$db = mysql_connect('localhost:3306', 'root', '');
        //mysql_select_db('symfony', $db);
        //parcours des photos et ajout dans la base
        while ($file = readdir($dir)) {
            if ($file != '.' && $file != '..' && !is_dir($file)) {
                $arrayChaine = explode('.', $file);

                if (count($arrayChaine) == 2) {

                    $nvllePhoto = new Image();
                    $nvllePhoto->setNom($arrayChaine[0]);
                    $nvllePhoto->setExtension($arrayChaine[1]);
                    $nvllePhoto->setAlt($arrayChaine[0]);
                    $nvllePhoto->setSrc($dirname . "/");

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($nvllePhoto);
                    $em->flush();
                }
            } else {
                echo 'Ne fonctionne pas pour ce repertoire : ' . $file . '<br>';
            }
        }
        //mysql_close();
    }

    /**
     * Ajoute recursivement les images contenu des repertoires
     * 
     * @param String path du repertoire
     */
    private function ajoutDirectoryPhotosBDD($dirname) {

        //ouverture du repertoire
        $dir = opendir($dirname);

        //boucle des repertoires
        while ($folder = readdir($dir)) {
            if (is_dir($dirname . $folder) && $folder != '.' && $folder != '..') {
                echo "avant appel ajoutphoto";
                $this->ajoutPhotos($dirname . $folder);
            }
        }
    }

    /**
     * resize les images contenu dans les repertoires fils du repertorie courant
     * @param String path du repertoire
     */
    private function reziserPhotosBDD($dirname) {
        $dir = opendir($dirname);
        $db = mysql_connect('localhost:3306', 'root', '');

// on sélectionne la base
        mysql_select_db('symfony', $db);

        while ($folder = readdir($dir)) {

            if (is_dir($dirname . $folder) && $folder != '.' && $folder != '..') {

                $folderOpen = opendir($dirname . $folder . '/');

                while ($file = readdir($folderOpen)) {
                    if ($file != '.' && $file != '..' && !is_dir($file)) {
                        $arrayChaine = explode('.', $file);
                        $img = new ImageResize($dirname . $folder . '/', $arrayChaine[0], '.' . $arrayChaine[1]);
                        $img->resize('homothety', $dirname . $folder . '/', 235, $arrayChaine[0] . '_min');
                    } else {
                        echo 'Ne fonctionne pas';
                    }
                }
            }
        }
    }

    public function loginAction() {

        $request = $this->getRequest();
        $session = $request->getSession();
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('JcgGutnerBundle:Default:login.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
        ));
    }

    //affiche les photos d'un reportage et permet le choix d'une photo
    //et la soumission d'un ofrmulaire qui modifie la photo de slide et de
    //presentation
    public function photoAction($url) {
        //on charge le reportage
        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Reportages");
        $reportage = $repository->findOneBy(array('url' => &$url));

        //on charge les images lie au reportages
        $image = $this->getDoctrine()
                ->getManager()
                ->getRepository("\Jcg\GutnerBundle\Entity\Image");

        $listeImages = $image->findBy(array('reportage' => $reportage->getId()));
        return $this->render("JcgGutnerBundle:Default:adminPhoto.html.twig", array("listeImages" => $listeImages, "reportage" => $reportage));
    }

    function actionAction($url) {
        switch ($url) {
            case "index":
                return $this->render('JcgGutnerBundle:Default:admin.html.twig');
                break;

            case "modifReportages":
                //chargement des reportages
                $repository = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("\Jcg\GutnerBundle\Entity\Reportages");
                $listReportages = $repository->findAll();
                return $this->render('JcgGutnerBundle:Default:adminReportages.html.twig', array("listReportages" => $listReportages));
                break;

            case "ajoutReportage":
                break;

            case "suppReportage":
                break;

            case "modifBiographie":
                $repository = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("\Jcg\GutnerBundle\Entity\Article");
                $biographie = $repository->findOneBy(array("is_bio" => 1));

                $form = $this->createFormBuilder($biographie)
                        ->add('titre', 'text')
                        ->add('contenu', 'textarea')
                        ->add('valider', 'submit')
                        ->getForm();

                $form->handleRequest($this->getRequest());
                if ($form->isValid()) {
                    $bio = $form->getData();
                    // sauvegarder le reportage tâche dans la bdd
                    $this->getDoctrine()->getManager()->persist($bio);
                    $this->getDoctrine()->getManager()->flush();
                    return $this->redirect($this->generateUrl('jcg_admin'));
                }

                return $this->render('JcgGutnerBundle:Default:adminBiographie.html.twig', array("form" => $form->createView(), "biographie" => $biographie));
                break;
            case "modifPresentationIndex":
                
                $article = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("\Jcg\GutnerBundle\Entity\Article");
                $presentation = $article->findOneBy(array("is_index" => 1));

                $form = $this->createFormBuilder($presentation)
                        ->add('contenu', 'textarea')
                        ->add('valider', 'submit')
                        ->getForm();
                
                if ($form->isValid()) {
                    $index = $form->getData();
                    // sauvegarder le reportage tâche dans la bdd
                    $this->getDoctrine()->getManager()->persist($index);
                    $this->getDoctrine()->getManager()->flush();
                    return $this->redirect($this->generateUrl('jcg_admin'));
                }
                return $this->render('JcgGutnerBundle:Default:adminIndex.html.twig', array("form" => $form->createView(), "presentation" => $presentation));
                break;
            default:
                $repository = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("\Jcg\GutnerBundle\Entity\Reportages");
                $reportage = $repository->findOneBy(array('url' => &$url));

                $form = $this->createFormBuilder($reportage)
                        ->add('intro', 'text')
                        ->add('url', 'text')
                        ->add('titre', 'text')
                        ->add('date', 'date')
                        ->add('contenu','textarea')
                        ->add('valider', 'submit')
                        ->getForm();

                $form->handleRequest($this->getRequest());
                if ($form->isValid()) {
                    // sauvegarder le reportage tâche dans la bdd
                    $this->getDoctrine()->getManager()->persist($reportage);
                    $this->getDoctrine()->getManager()->flush();
                    return $this->redirect($this->generateUrl('jcg_admin'));
                }


                return $this->render('JcgGutnerBundle:Default:adminReportage.html.twig', array("reportage" => $reportage, 'form' => $form->createView()));
        }
    }

//fin class
}

//ajoutPhotosBDD($dirnameReportage);
//ajoutPhotos("images");
//ajoutDirectoryPhotosBDD("images/reportages/");


