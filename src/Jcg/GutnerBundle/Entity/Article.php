<?php

namespace Jcg\GutnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Jcg\GutnerBundle\Entity\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @ORM\Column(name="is_bio", type="boolean")
     */
    private $is_bio;
    
    /**
     * @ORM\Column(name="is_index", type="boolean")
     */
    private $is_index;
    
    /**
     * Get id
     *
     * @return integer 
     */
   
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    
        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set is_bio
     *
     * @param boolean $isBio
     * @return Article
     */
    public function setIsBio($isBio)
    {
        $this->is_bio = $isBio;
    
        return $this;
    }

    /**
     * Get is_bio
     *
     * @return boolean 
     */
    public function getIsBio()
    {
        return $this->is_bio;
    }

    /**
     * Set is_index
     *
     * @param boolean $isIndex
     * @return Article
     */
    public function setIsIndex($isIndex)
    {
        $this->is_index = $isIndex;
    
        return $this;
    }

    /**
     * Get is_index
     *
     * @return boolean 
     */
    public function getIsIndex()
    {
        return $this->is_index;
    }
}