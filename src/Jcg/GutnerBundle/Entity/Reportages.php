<?php

namespace Jcg\GutnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reportages
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Jcg\GutnerBundle\Entity\ReportagesRepository")
 */
class Reportages {

   /**
   * @ORM\OneToOne(targetEntity="Jcg\GutnerBundle\Entity\Image", cascade={"persist"})
   */
    private $imageFront;
    
    /**
   * @ORM\OneToOne(targetEntity="Jcg\GutnerBundle\Entity\Image", cascade={"persist"})
   */
    private $imageSlide;
    
   /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=150)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="string", length=150)
     */
    private $intro;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbAffichage", type="integer")
     */
    private $nbAffichage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Reportages
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Reportages
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Reportages
     */
    public function setContenu($contenu) {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu() {
        return $this->contenu;
    }

    /**
     * Set nbAffichage
     *
     * @param integer $nbAffichage
     * @return Reportages
     */
    public function setNbAffichage($nbAffichage) {
        $this->nbAffichage = $nbAffichage;

        return $this;
    }

    /**
     * Get nbAffichage
     *
     * @return integer 
     */
    public function getNbAffichage() {
        return $this->nbAffichage;
    }

    /**
     * Get image
     *
     * @return \Jcg\GutnerBundle\Entity\Image
     */
    public function getImage(){
        return $this->imageFront;
    }
    
    /**
     * Set Image
     * 
     * @param \Jcg\GutnerBundle\Entity\Image $image
     * @return \Jcg\GutnerBundle\Entity\Reportages
     */
    public function setImage($image){
        $this->imageFront = $image;
        return $this;
    }
    
    /**
     * Get ImageSlide
     *
     * @return \Jcg\GutnerBundle\Entity\Image
     */
    public function getImageSlide(){
        return $this->imageSlide;
    }
    
    /**
     * Set ImageSlide
     * 
     * @param \Jcg\GutnerBundle\Entity\Image $image
     * @return \Jcg\GutnerBundle\Entity\Reportages
     */
    public function setImageSlide($image){
        $this->imageSlide = $image;
        return $this;
    }
    
    
    /**
     * 
     * @param string $intro
     * @return \Jcg\GutnerBundle\Entity\Reportages
     */
    public function setIntro($intro){
        $this->intro = $intro;
        return $this;
    }
    
    
    /**
     * 
     * @return string
     */
    public function getIntro(){
        return $this->intro;
    }
    
        /**
     *
     * Set Url 
     * 
     * @param string $url
     * @return \Jcg\GutnerBundle\Entity\Reportages
     */
    public function setUrl($url){
        $this->url = $url;
        return $this;
    }
    
    
    /**
     * 
     * @return string
     */
    public function getUrl(){
        return $this->url;
    }
}
