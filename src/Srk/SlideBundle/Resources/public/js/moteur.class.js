function moteur(compteur) {
    this.compteur = compteur;

}
moteur.prototype.next = function(valeur) {
    if (valeur === undefined)
        return this.compteur + 1;
    else
        return this.compteur + valeur;
}
moteur.prototype.prec = function(valeur) {
    if (valeur === undefined)
        return this.compteur - 1;
    else
        return this.compteur - valeur;
}
moteur.prototype.current = function() {
    return this.compteur;
}
moteur.prototype.set = function(valeur) {
    return this.compteur = valeur;
}