<?php

namespace Srk\SlideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SrkSlideBundle:Default:index.html.twig', array('name' => $name));
    }
}
