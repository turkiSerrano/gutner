$(function() {

    var n = 0;
    var start = setInterval(moveSlider, 5000);

    $('#precedent').click(function() {

        if (n == 0) {

            stopInterval();
            move(false, 1592);
            n++;
        }
        else {
            stopInterval();
            move(true, 796);

        }
    });

    $('#suivant').click(function() {

        if (n == 2) {
            stopInterval();
            move(true, 1592);
            n--;

        }
        else {
            stopInterval();
            move(false, 796);
        }
    })


    $('#slides').mouseenter(stopInterval);

    $('#slides').mouseleave(restart);

    $('#listeNavigation nav ul li').click(function() {

        var btClick = $(this).index();
        var currentBt = n;

        var delta = currentBt - btClick;
        if (delta > 0)
            move(true, delta * 796)
        else
            move(false, Math.abs(delta * 796));
        n = btClick;
    })

    function moveSlider() {

        if (n == 2) {
            move(true, 1592);
            n = 0;
        }
        else {
            move(false, 796);
        }
    }
    function stopInterval() {
        clearInterval(start);

    }
    function restart() {
         setTimeout(function(){
            start = setInterval(moveSlider, 3000);
        },2000)
        

    }

    function move(sens, delta) {

        if (sens) {
            $('#contenantSlider').animate({left: '+=' + delta},{ duration:1800, easing:'easeOutCubic'});
            n--;
        }
        else {
            $('#contenantSlider').animate({left: '-=' + delta},{ duration:1800, easing:'easeOutCubic'});
            n++;
        }
    }

})

